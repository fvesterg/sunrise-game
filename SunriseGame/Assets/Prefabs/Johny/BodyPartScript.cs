﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartScript : MonoBehaviour
{
    public static BodyPartScript Instance { get; private set; }

    public enum Place
    {
        head, body, feet, hand
    }

    public Transform head;
    public Transform body;
    public Transform footLeft;
    public Transform footRight;
    public Transform hand;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject PlaceItem(ShopItem item)
    {
        var itemObject = Instantiate(item.prefab, GetBone(item.place));

        return itemObject;
    }

    private Transform GetBone(Place place)
    {
        switch (place)
        {
            case Place.head:
                return head;
            case Place.body:
                return body;
            case Place.hand:
                return hand;
            default:
                throw new NotImplementedException();
        }
    }
}
