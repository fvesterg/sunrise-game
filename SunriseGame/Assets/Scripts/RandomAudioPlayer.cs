﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioPlayer : MonoBehaviour
{
    public bool autoPlay = true;
    public AudioSource source;
    public AudioClip[] clips;

    float pitchBase;
    public float pitchVariation = 0.1f;
    float volumeBase;
    public float volumeVariation = 0.05f;

    public float minDelay = 2;
    public float maxDelay = 3;

    float time = 0;

    private void Start()
    {
        pitchBase = source.pitch;
        volumeBase = source.volume;

        time = Random.Range(0, maxDelay);
    }

    private void SetTime()
    {
        time = Random.Range(minDelay, maxDelay);
    }

    private void Update()
    {
        if (!autoPlay)
            return;

        if (!source.isPlaying)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
            }
            else
            {
                PlaySound();
                SetTime();
            }
        }
    }

    public void PlaySound()
    {
        var nextClip = clips[Random.Range(0, clips.Length)];
        source.clip = nextClip;
        source.pitch = pitchBase + Random.Range(-pitchVariation, pitchVariation);
        source.volume = volumeBase + Random.Range(-volumeVariation, volumeVariation);

        source.Play();
    }
}
