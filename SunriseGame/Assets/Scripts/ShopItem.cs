﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Shop Item", menuName = "Shop Item")]
public class ShopItem : ScriptableObject
{
    public BodyPartScript.Place place;

    //public int neededAmount = 10;     //Will be determined by the library
    public Sprite icon;
    public GameObject prefab;
}
