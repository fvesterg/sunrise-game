﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeRecognizer : MonoBehaviour
{
    public SlideAnimation slideablePanel;

    public int minPixelDelta = 64;

    public Vector2 openDirection = Vector2.right;
    public float maxAngle = 45;

    float dotMin;

    RectTransform rectTransform { get => transform as RectTransform; }

    Vector2 pointerOrigin;
    bool checking;
    int touchIndex = -1;

    private void Start()
    {
        dotMin = Mathf.Cos(Mathf.Deg2Rad * maxAngle);
    }

    private void Update()
    {
        if (checking)
        {
            if (touchIndex == -1)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    CheckSwipe((Vector2)Input.mousePosition - pointerOrigin);
                    checking = false;
                    pointerOrigin = Vector2.one * 1000;
                }
            }
            else
            {
                var touch = Input.GetTouch(touchIndex);
                CheckSwipe(touch.position - pointerOrigin);

                if (touch.phase == TouchPhase.Ended ||touch.phase == TouchPhase.Canceled)
                {
                    checking = false;
                    touchIndex = -1;
                }
            }
        }
        else
        {
            bool trigger = false;
            if (Input.GetMouseButtonDown(0))
            {
                pointerOrigin = Input.mousePosition;
                trigger = true;
            }
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    var touch = Input.touches[i];
                    if (touch.phase == TouchPhase.Began)
                    {
                        pointerOrigin = touch.position;
                        touchIndex = i;
                        trigger = true;
                        break;
                    }
                }
            }

            if (trigger)
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(rectTransform, pointerOrigin))
                {
                    checking = true;
                }
                else
                {
                    touchIndex = -1;
                }
            }
        }
    }

    private void CheckSwipe(Vector2 delta)
    {
        float mag = delta.magnitude;
        if (mag >= minPixelDelta)
        {
            var direction = delta / mag;

            float dot = Vector2.Dot(openDirection, direction);

            if (dot > 0)
            {
                if (dot >= dotMin)
                    slideablePanel.Open();
            }
            else
            {
                dot *= -1;
                if (dot >= dotMin)
                    slideablePanel.Close();
            }
        }
    }
}
