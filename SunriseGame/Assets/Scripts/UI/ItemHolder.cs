﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemHolder : MonoBehaviour
{
    public Button button;
    public ShopItem item;
    public GameObject objectReference;

    [Space(10)]
    public Image icon;

    public bool Empty { get => item == null; }

    public void AddItem(ShopItem item)
    {
        Debug.Assert(Empty);

        button.interactable = true;

        this.item = item;
        icon.sprite = item.icon;
        icon.gameObject.SetActive(true);
        objectReference = BodyPartScript.Instance.PlaceItem(item);

        CurrentItems.ItemsChanged.Invoke();
    }

    public void RemoveItem()
    {
        button.interactable = false;

        item = null;
        icon.sprite = null;
        icon.gameObject.SetActive(false);
        Destroy(objectReference);

        CurrentItems.ItemsChanged.Invoke();
    }
}
