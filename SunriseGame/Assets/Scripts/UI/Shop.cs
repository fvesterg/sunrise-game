﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public static Shop Instance { get; private set; }

    public int currentLevel = 1;
    public Level[] levels;

    public ShopItemPanel panel1;
    public ShopItemPanel panel2;
    public ShopItemPanel panel3;
    public ShopItemPanel panel4;

    public Button previousLevelButton;
    public Button nextLevelButton;

    [Space(10)]
    public ParticleSystem ps;
    public RandomAudioPlayer applause;
    public UpdateIndicator shopUpdate;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        UpdateShop();
        HighFiveCounter.OnCounterUpdate += CheckUnlock;
    }

    public void ShowNextLevel()
    {
        currentLevel++;
        UpdateShop();
    }
    public void ShowPreviousLevel()
    {
        currentLevel--;
        UpdateShop();
    }

    private void UpdateShop()
    {
        var level = levels[currentLevel - 1];

        int itemCost1 = CalculateItemCost(0);
        int itemCost2 = CalculateItemCost(1);
        int itemCost3 = CalculateItemCost(2);
        int itemCost4 = CalculateItemCost(3);

        panel1.SetItem(level.item1, itemCost1);
        panel2.SetItem(level.item2, itemCost2);
        panel3.SetItem(level.item3, itemCost3);
        panel4.SetItem(level.item4, itemCost4);

        previousLevelButton.interactable = currentLevel > 1;
        nextLevelButton.interactable = currentLevel - 1 < (HighFiveCounter.Count / 50);
    }

    private int CalculateItemCost(int itemIndex, int currentLevel = -1)
    {
        currentLevel = (currentLevel == -1) ? this.currentLevel : currentLevel;

        int baseCost = (currentLevel - 1) * 50;
        return baseCost + (itemIndex + 1) * 10;
    }

    private void CheckUnlock(int newCount)
    {
        int levelIndex = newCount / 50;
        int itemTier = (int)Mathf.Clamp((newCount - levelIndex * 50) / 10f, 0, 4);

        if (levelIndex < levels.Length && itemTier > 0)
        {
            int neededAmount = CalculateItemCost(itemTier - 1, levelIndex + 1);
            if (neededAmount == newCount)
            {
                ps.Play();
                applause.PlaySound();
                shopUpdate.Activate();
            }
        }

        UpdateShop();
    }

    public ShopItemPanel GetPanel(int itemTier)
    {
        switch (itemTier)
        {
            case 1:
                return panel1;
            case 2:
                return panel2;
            case 3:
                return panel3;
            case 4:
                return panel4;
            default:
                return null;
        }
    }

    [Serializable]
    public struct Level
    {
        public ShopItem item1;
        public ShopItem item2;
        public ShopItem item3;
        public ShopItem item4;
    }
}
