﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemPanel : MonoBehaviour
{
    public ShopItem item;

    public Text counter;
    public Image img;

    public Button buyButton;

    public int neededAmount;
    bool _unlocked;
    public bool Unlocked
    {
        get => _unlocked;
        set
        {
            _unlocked = value;
            buyButton.interactable = value;
        }
    }

    private void UpdateCounter(int count)
    {
        counter.text = Mathf.Min(count, neededAmount) + "/" + neededAmount;

        if (!Unlocked)
        {
            Unlocked = count >= neededAmount;
        }
    }

    internal void SetItem(ShopItem item, int neededAmount)
    {
        Unlocked = false;

        if (item != null)
        {
            this.item = item;

            img.sprite = item.icon;
            this.neededAmount = neededAmount;

            UpdateCounter(HighFiveCounter.Count);
        }
        else
        {
            this.item = null;
            this.neededAmount = 1000;
            img.sprite = null;
            UpdateCounter(HighFiveCounter.Count);
        }
    }

    public void SubscribeUpdate()
    {
        HighFiveCounter.OnCounterUpdate += UpdateCounter;
        UpdateCounter(HighFiveCounter.Count);
    }
    public void UnsubrscribeUpdate()
    {
        HighFiveCounter.OnCounterUpdate -= UpdateCounter;
    }

    public void TryAddItem()
    {
        CurrentItems.Instance.AddItem(item);
    }
}
