﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SlideAnimation : MonoBehaviour
{
    public bool open;

    [Space(10)]
    public float duration = .5f;
    public Vector2 openPosition;
    public Vector2 closePosition;

    TweenerCore<Vector2, Vector2, VectorOptions> currentTween;

    public RectTransform rectTransform { get => transform as RectTransform; }

    public UnityEvent OnOpen;
    public UnityEvent OnClose;


    public void Trigger()
    {
        if (!open) Open();
        else Close();
    }

    public void Open()
    {
        open = true;
        KillCurrentTween();

        currentTween = rectTransform.DOAnchorPos(openPosition, duration);
        OnOpen?.Invoke();
    }

    public void Close()
    {
        open = false;
        KillCurrentTween();

        currentTween = rectTransform.DOAnchorPos(closePosition, duration);
        OnClose?.Invoke();
    }

    private void KillCurrentTween()
    {
        if (currentTween != null)
        {
            currentTween.Kill();
        }
    }
}
