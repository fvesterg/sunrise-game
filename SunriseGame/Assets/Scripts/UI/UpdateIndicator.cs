﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateIndicator : MonoBehaviour
{
    public Button button;
    public SlideAnimation slideAnimation;

    private void Start()
    {
        button = GetComponent<Button>();
    }

    public void Activate()
    {
        var colors = button.colors;
        colors.normalColor = Color.red;
        button.colors = colors;

        slideAnimation.OnOpen.AddListener(SetNormalColor);
    }

    void SetNormalColor()
    {
        var colors = button.colors;
        colors.normalColor = Color.white;
        button.colors = colors;

        slideAnimation.OnOpen.RemoveListener(SetNormalColor);
    }
}
