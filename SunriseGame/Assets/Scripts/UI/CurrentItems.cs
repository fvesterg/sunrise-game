﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentItems : MonoBehaviour
{
    public delegate void PanelEvent();
    public static PanelEvent ItemsChanged;
    public static CurrentItems Instance { get; private set; }

    public SlideAnimation slideAnimation;
    public ItemHolder[] holders;

    public bool IsFull { get; private set; }
    public bool IsEmpty { get; private set; }

    private void Awake()
    {
        Instance = this;
        ItemsChanged += Open;
    }

    private void Open()
    {
        CheckIsFull();

        if (IsEmpty)
            slideAnimation.Close();
        else
            slideAnimation.Open();
    }

    public void AddItem(ShopItem item)
    {
        if (IsFull)
            return;

        foreach (var holder in holders)
        {
            if (holder.item == item)
                return;
        }


        foreach (var holder in holders)
        {
            if (holder.Empty)
            {
                holder.AddItem(item);
                CheckIsFull();
                return;
            }
        }
    }

    private void CheckIsFull()
    {
        IsEmpty = true;
        foreach (var holder in holders)
        {
            if (!holder.Empty)
            {
                IsEmpty = false;
                break;
            }
        }

        foreach (var holder in holders)
        {
            if (holder.Empty)
            {
                IsFull = false;
                return;
            }
        }
        IsFull = true;
    }
}
