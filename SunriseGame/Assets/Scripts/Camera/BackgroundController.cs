﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BackgroundController : MonoBehaviour
{
    public static BackgroundController Instance { get; private set; }

    public new Camera camera;
    public Material shader;
    public Material TrailMaterial;
    public float animationLength;

    public Color backgroundColor;
    Color starColor;

    int colorIndex = 0;
    public Color[] colorLibrary;

    private void Awake()
    {
        Instance = this;

        backgroundColor = colorLibrary[0];
        shader.SetColor("_Color1", backgroundColor);
        shader.SetColor("_Color2", backgroundColor);
    }

    private void Start()
    {
        shader.SetFloat("_ScreenRatio", Screen.width / (float)Screen.height);
        shader.SetInt("_ScreenWidth", Screen.width);
        shader.SetInt("_StarRadius", (int)(Screen.height * 1.05f));
        shader.SetInt("_StarRadiusInner", Screen.height);
    }

    public void StartAnimation(Vector3 worldPosition)
    {
        Vector2 screenPos = camera.WorldToScreenPoint(worldPosition);
        StartAnimation(screenPos);
    }
    public void StartAnimation(Vector2 screenPosition)
    {
        StopCoroutine(AnimationCoroutine());

        int newIndex = -1;
        while (newIndex == colorIndex || newIndex == -1)
            newIndex = Random.Range(0, colorLibrary.Length);

        backgroundColor = colorLibrary[colorIndex];
        starColor = colorLibrary[newIndex];
        TrailMaterial.color = colorLibrary[newIndex % colorLibrary.Length];

        colorIndex = newIndex;

        shader.SetColor("_Color1", backgroundColor);
        shader.SetColor("_Color2", starColor);
        shader.SetFloat("_EffectTime", 0);

        shader.SetVector("_Screen", screenPosition);

        StartCoroutine(AnimationCoroutine());
    }

    private IEnumerator AnimationCoroutine()
    {
        float time = 0;
        while (time < animationLength)
        {
            yield return new WaitForEndOfFrame();
            time += Time.deltaTime;

            shader.SetFloat("_EffectTime", Mathf.Clamp01(time / animationLength));
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, shader);
    }
}
