﻿using System.Collections;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public int minEnergy = -3;
    public int maxEnergy = 3;
    int _energy;
    int Energy
    {
        get => _energy;
        set
        {
            _energy = Mathf.Clamp(value, minEnergy, maxEnergy);
            CheckEnergy();
        }
    }

    bool loadingUp;
    bool awake = true;
    float yelpTime;

    public Animator johnnyAnimator;
    public RandomAudioPlayer audioPlayer;

    public LayerMask touchMask;
    public static TouchController Instance { get; private set; }

    Collider2D lastCollider;
    HighFiveTarget lastTouchTarget;

    private void Awake()
    {
        Instance = this;
        _energy = maxEnergy;
    }

    private void Update()
    {
        yelpTime += Time.deltaTime;

        if (awake && yelpTime > 15)
        {
            Yelp();
        }

        if (Debug.isDebugBuild)
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                lastTouchTarget.Trigger();

                HighFiveCounter.Count += 8;
                johnnyAnimator.SetTrigger("HighFive");
                Energy += 5;

            }
        }
    }

    private void Start()
    {
        StartCoroutine(AddEnergyCoroutine());
    }

    public void SendTouchSignal(Vector2 screenPosition)
    {
        if (awake)
        {
            Collider2D collider = Physics2D.OverlapPoint(screenPosition, touchMask);
            if (collider != null)
            {
                HighFiveTarget touchTarget;

                if (collider != lastCollider)
                {
                    touchTarget = collider.GetComponent<HighFiveTarget>();
                    lastCollider = collider;
                }
                else
                    touchTarget = lastTouchTarget;

                touchTarget.Trigger();

                johnnyAnimator.SetTrigger("HighFive");

                lastTouchTarget = touchTarget;
                Energy--;
            }
        }
    }

    public IEnumerator AddEnergyCoroutine()
    {
        loadingUp = true;
        while (Energy < maxEnergy)
        {
            yield return new WaitForSeconds(Random.Range(8, 10));

            Energy++;

            if (awake)
            {
                Yelp();
            }
        }
        loadingUp = false;
    }

    private void Yelp()
    {
        johnnyAnimator.SetFloat("TrollBlend", Random.Range(0f, 1f));
        johnnyAnimator.SetTrigger("Troll");
        audioPlayer.PlaySound();
        yelpTime = 0;
    }

    public void WakeUp()
    {
        awake = true;
    }

    private void CheckEnergy()
    {
        if (awake)
        {
            if (Energy <= minEnergy)
            {
                awake = false;
                johnnyAnimator.SetBool("Tired", true);
            }
        }
        else
        {
            if (Energy >= 0)
            {
                awake = true;
                johnnyAnimator.SetBool("Tired", false);
            }
        }
        if (Energy <= 0 && !loadingUp)
            StartCoroutine(AddEnergyCoroutine());
    }
}
