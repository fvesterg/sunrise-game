﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class HighFiveTarget : MonoBehaviour
{
    public RandomAudioPlayer clapAudioPlayer;
    public RandomAudioPlayer yeahAudioPlayer;

    public ParticleSystem ps;

    private void Awake()
    {
        ps.Stop();
    }

    internal void Trigger()
    {
        HighFiveCounter.Instance.Add();
        BackgroundController.Instance.StartAnimation(transform.position);

        clapAudioPlayer.PlaySound();
        yeahAudioPlayer.PlaySound();
        ps.Play();
    }
}
