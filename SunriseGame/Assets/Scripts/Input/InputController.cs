﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class InputController : MonoBehaviour
{
#pragma warning disable CS0649
    [SerializeField] new Camera camera;
#pragma warning restore CS0649
    public Vector2 CursorPosition { get; private set; }

    private void Update()
    {
        bool click = false;

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            CursorPosition = touch.position;
            click = touch.phase == TouchPhase.Began;
        }
        else
        {
            CursorPosition = Input.mousePosition;
            click = Input.GetMouseButtonDown(0);
        }

        if (click)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                TouchController.Instance.SendTouchSignal(camera.ScreenToWorldPoint(CursorPosition));
            }
        }
    }
}
