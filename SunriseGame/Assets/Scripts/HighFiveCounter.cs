﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighFiveCounter : MonoBehaviour
{
    public delegate void CounterUpdate(int newCount);
    public static CounterUpdate OnCounterUpdate;
    public static HighFiveCounter Instance { get; private set; }

    public Text counterPanel;

    static int _count;
    public static int Count
    {
        get => _count;
        set
        {
            _count = value;
            OnCounterUpdate?.Invoke(value);
            Instance.counterPanel.text = value.ToString();
        }
    }

    private void Awake()
    {
        Instance = this;

        UpdatePanel(Count);
    }

    public void Add()
    {
        Count++;
    }

    private void UpdatePanel(int newCount)
    {
        counterPanel.text = newCount.ToString();
    }

    private void OnEnable()
    {
        OnCounterUpdate += UpdatePanel;
    }
    private void OnDisable()
    {
        OnCounterUpdate -= UpdatePanel;
    }
}
