﻿Shader "Custom/Background Texture Effect StarSwipe"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

        _ReplaceColor("Green Screen Color", Color) = (0,1,0,1)
        _ReplaceThreshold("Replacement Threshold", Range(0,0.999)) = 0.5
        _Color1("Background Color", Color) = (1,1,1,1)
        _Color2("Star Color", Color) = (0,0,0,1)

        //xy: Star Center, z: Screen Ratio
        _Screen("Screen Options", Vector) = (960, 540, 0, 0)
        _ScreenRatio("Screen Ratio", float) = 0.5625
        _ScreenWidth("ScreenWidth", int) = 1080
        _StarRadius("Star Radius", int) = 540
        _StarRadiusInner("Inner Star Radius", int) = 500
        _Points("Star Points", int) = 36

        _EffectTime("Time", Range(0,1)) = 0.5
        _EffectTurn("Effect Rotation", float) = 90
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 _Color1;
            fixed4 _Color2;

            float4 _Screen;
            float _ScreenRatio;
            uint _ScreenWidth;
            uint _StarRadius;
            uint _StarRadiusInner;
            uint _Points;

            float _EffectTime;
            float _EffectTurn;

            float3 GetStarColor(float2 uv) {
                int x = uv.x * _ScreenWidth - _Screen.x;
                int y = uv.y * _ScreenWidth / _ScreenRatio - _Screen.y;

                float radius = sqrt(pow(x, 2) + pow(y, 2));

                int deltaRadius = (_StarRadius - _StarRadiusInner) / 2;
                float angle = atan(y / float(x)) + _EffectTurn / 360.0 * _EffectTime;

                float totalAngle = 2 * 360.0 / _Points;
                angle = angle % totalAngle;

                int finalRadius = (_StarRadiusInner + deltaRadius) + sin(angle * _Points) * deltaRadius;
                finalRadius *= _EffectTime;

                float t = clamp(floor(radius / finalRadius), 0, 1);

                return t * _Color1 + (1 - t) * _Color2;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = fixed4(GetStarColor(i.uv), 1);
                return col;
            }
            ENDCG
        }
    }
}
